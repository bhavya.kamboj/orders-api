package com.classpath.ordersapi.model;

import java.time.LocalDate;

public interface OrderDto {

	LocalDate getOrderDate();

	String getFirstName();

	String getLastName();

	String getEmail();

	double getPrice();

	default String getFullName() {
		return getFirstName().concat(" ").concat(getLastName());
	}
}
