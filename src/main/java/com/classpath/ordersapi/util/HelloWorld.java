package com.classpath.ordersapi.util;

import static java.util.stream.Stream.of;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("dev")
public class HelloWorld implements CommandLineRunner {
	
	private final ApplicationContext applicationContext;
	
	//@Autowired - This is not necessary with constructor dependency injection
	public HelloWorld(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public void run(String... args) throws Exception {
		//System.out.println("Hello world from Spring Boot-2 !!");
		String []beanNamesArray = this.applicationContext.getBeanDefinitionNames();
		
		System.out.println("********************");
		of(beanNamesArray)
		.filter(bean -> bean.startsWith("user"))
		.forEach(bean -> System.out.println(bean));
		System.out.println("********************");
	}
}
