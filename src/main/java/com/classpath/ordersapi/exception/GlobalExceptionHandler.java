package com.classpath.ordersapi.exception;

import static java.util.stream.Collectors.toSet;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.Set;

import org.springframework.context.annotation.Configuration;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseStatus(NOT_FOUND)
	public Error handleInvalidOrderId(IllegalArgumentException exception) {
		log.error("Invalid order id passed:: {}", exception.getMessage());
		return new Error(exception.getMessage());
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(BAD_REQUEST)
	public Set<Error> handleInvalidData(MethodArgumentNotValidException exception){
		log.error("invalid input data");
		return exception.getAllErrors()
				.stream()
				.map(ObjectError:: getDefaultMessage)
				.map(Error::new)
				.collect(toSet());
	}

}

@AllArgsConstructor
@Getter
class Error{
	private String message;
}
