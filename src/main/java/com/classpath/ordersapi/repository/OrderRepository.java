package com.classpath.ordersapi.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.model.OrderDto;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	
	Page<OrderDto> findBy(Pageable pageRequest);
	
	Page<Order> findByOrderDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageRequest);
	
	Page<Order> findByPriceBetween(double minPrice, double maxPrice, Pageable pageRequest);

}
