package com.classpath.ordersapi.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import com.classpath.ordersapi.repository.OrderRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;

	@Override
	public Health health() {
		try {
			long count = this.orderRepository.count();
			log.info("Number of orders :: {}", count);
			return Health.down().withDetail("DB", "DB is healthy").build();
		} catch (Exception exception) {
			log.error("error :: {} ", exception.getMessage());
			return Health.down().withDetail("DB", exception.getMessage()).build();
		}
	}
}

@Component
@RequiredArgsConstructor
@Slf4j
class KafkaHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;

	@Override
	public Health health() {
		try {
			long count = this.orderRepository.count();
			log.info("Number of orders :: {}", count);
			return Health.up().withDetail("Kafka", "kafka is healthy").build();
		} catch (Exception exception) {
			log.error("error :: {} ", exception.getMessage());
			return Health.down().withDetail("Kafka", exception.getMessage()).build();
		}
	}
}

@Component
@RequiredArgsConstructor
@Slf4j
class PaymentGatewayHealthIndicator implements HealthIndicator {

	private final OrderRepository orderRepository;

	@Override
	public Health health() {
		try {
			long count = this.orderRepository.count();
			log.info("Number of orders :: {}", count);
			return Health.up().withDetail("PaymentGateway", "PaymentGateway is healthy").build();
		} catch (Exception exception) {
			log.error("error :: {} ", exception.getMessage());
			return Health.down().withDetail("PaymentGateway", exception.getMessage()).build();
		}
	}
}
