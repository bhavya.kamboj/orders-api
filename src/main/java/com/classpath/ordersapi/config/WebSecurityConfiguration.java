package com.classpath.ordersapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationProvider;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity.cors().disable();
		httpSecurity.csrf().disable();
		httpSecurity.headers().frameOptions().disable();
		
		httpSecurity
			.authorizeRequests()
			.antMatchers("/h2-console/**", "/actuator/**")
				.permitAll()
			.antMatchers(HttpMethod.GET, "/api/v1/orders/**")
				.hasAnyRole("Everyone", "super_admins", "admins")
			.antMatchers(HttpMethod.POST, "/api/v1/orders/**")
				.hasAnyRole("super_admins", "admins")
			.antMatchers(HttpMethod.DELETE, "/api/v1/orders/**")
				.hasAnyRole("super_admins")
			.and()
			.oauth2ResourceServer()
			.jwt();
	}
	
	@Bean
	public JwtAuthenticationConverter jwtAuthenticationConverter() {
		JwtAuthenticationConverter jwtAuthenticationConverted = new JwtAuthenticationConverter();
		JwtGrantedAuthoritiesConverter grantedAuthConverter = new JwtGrantedAuthoritiesConverter();
		grantedAuthConverter.setAuthoritiesClaimName("groups");
		grantedAuthConverter.setAuthorityPrefix("ROLE_");
		jwtAuthenticationConverted.setJwtGrantedAuthoritiesConverter(grantedAuthConverter);
		return jwtAuthenticationConverted;
	}
}
